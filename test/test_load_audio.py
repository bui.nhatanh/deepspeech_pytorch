import argparse

from datasets.data_loader import SpectrogramParser
from helpers.opts import add_inference_args

audio_conf = dict(sample_rate=22050,
                  window_size=.02,
                  window_stride=.01,
                  window='hamming'
                  )

parser = argparse.ArgumentParser(description='DeepSpeech transcription')
parser = add_inference_args(parser)
parser.add_argument('--audio-path',
                    default="/home/anhbn/PycharmProjects/japanese-speech-recognition/datasets/corpus/meian/rsr_wav"
                            "/meian_0001.wav",
                    help='Audio file to predict on')
parser.add_argument('--offsets', dest='offsets', action='store_true', help='Returns time offset information')
args = parser.parse_args()

audio_path = args.audio_path
parser = SpectrogramParser(audio_conf, normalize=True)
spect = parser.parse_audio(audio_path).contiguous()
spect = spect.view(1, 1, spect.size(0), spect.size(1))
