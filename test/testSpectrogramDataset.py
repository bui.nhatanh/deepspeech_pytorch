from unittest import TestCase

from data.data_loader import SpectrogramDataset, AudioDataLoader, BucketingSampler

audio_conf = dict(sample_rate=22050,
                  window_size=.02,
                  window_stride=.01,
                  window='hamming'
                  )
manifest_filepath = '/home/anhbn/PycharmProjects/deepspeech/data/metadata/meian_manifest_train.csv'
labels = [
    "'",
    "-",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "m",
    "n",
    "o",
    "p",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "―",
    "、",
    "。",
    "？",
    " "
]
normalize = True


class TestSpectrogramDataset(TestCase):
    def test_1(self):
        train_dataset = SpectrogramDataset(audio_conf=audio_conf, manifest_filepath=manifest_filepath,
                                           labels=labels, normalize=True, augment=False)
        train_dataset.parse_transcript("/home/anhbn/PycharmProjects/deepspeech/data/corpus/meian/txt/meian_0000.txt")
        train_sampler = BucketingSampler(train_dataset, batch_size=20)
        train_loader = AudioDataLoader(train_dataset,
                                       num_workers=4, batch_sampler=train_sampler)
        print(0)
